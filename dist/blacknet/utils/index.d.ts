declare class Utils {
    static EncodeVarInt: typeof EncodeVarInt;
    static NumberOfLeadingZeros: typeof NumberOfLeadingZeros;
    static DecodeVarInt: typeof DecodeVarInt;
    static HexToPublicKey: typeof HexToPublicKey;
    static PublicKeyToHex: typeof PublicKeyToHex;
    static PublicKey: typeof PublicKey;
    static PublicKeyToAccount: typeof PublicKeyToAccount;
    static ToHex: typeof ToHex;
    static ToByte: typeof ToByte;
}
export default Utils;
export declare function PublicKey(account: string): Uint8Array;
export declare function PublicKeyToAccount(publicKey: Uint8Array): string;
export declare function PublicKeyToHex(arr: Uint8Array): string;
export declare function HexToPublicKey(str: string): Uint8Array;
export declare function ToHex(arr: Uint8Array): string;
export declare function ToByte(str: string): Uint8Array;
export declare function EncodeVarInt(value: number): Uint8Array;
export declare function DecodeVarInt(arr: Uint8Array): number;
export declare function NumberOfLeadingZeros(i: number): number;
