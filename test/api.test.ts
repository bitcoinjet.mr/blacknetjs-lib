import blacknetjs from '../src';

const testAccount = 'blacknet14w6tm25y7rt24zj7r8fq7rnzd50qtpgmpfwv50r7qjnqhcwlxszqanh036';
// const testMnemonic = 'piano maze provide discover tower scissors true leave senior aware secret film';
const testMnemonic = '1e662e8fc3df898cc86777af5b3711c5115ed49c302d47fcb164cea5f18ea2c7abb4bdaa84f0d6aa8a5e19d20f0e626d1e05851b0a5cca3c7e04a60be1df3404';
const testMessage = 'BLN-is-very-nice';
const testSign = 'FF6D74C0493720F59DA4F06CD6D13D4A47D04F61E6D4AFF3D001EBD59153DC6DF40DDA34F7CD63FBA76CD2C1CA3963D1CB4F17F2061FB191BA441F0BF925C40B';
const testSerialized = '00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000abb4bdaa84f0d6aa8a5e19d20f0e626d1e05851b0a5cca3c7e04a60be1df34040000000098142b000347fb0fb10c15c6136b759af8963c46d7bcd74f1bbc18ca22dcec1700000000000186a000aa000000003b9aca001c6c6e21ce7d9a16892753d801b778549692aa23f0655ba83a5c46e7475cb5f30080';
const testSignature = '7cad618727b1dd3872d685c4c2eba86843e64321fe622a60a66807e5a495a7b081a04eb8eebd84b683d2cbad8b13ce6277b4e2280ec6da528d0b9f344f246a0babb4bdaa84f0d6aa8a5e19d20f0e626d1e05851b0a5cca3c7e04a60be1df34040000000098142b000347fb0fb10c15c6136b759af8963c46d7bcd74f1bbc18ca22dcec1700000000000186a000aa000000003b9aca001c6c6e21ce7d9a16892753d801b778549692aa23f0655ba83a5c46e7475cb5f30080'
const testEncrypted = '669E238CBD3977550DEAB18DEE256BCB16748CA9';
const testStr       = 'blacknet';
// const testMnemonic2 = "prepare long erode easy moment dinosaur soft sound exhibit wire mesh muffin";
const testMnemonic2 = "1fff08886d7de93b20e9217891a333c58c6765032c8ba5ed74ddc6a9d4aab5550ce115862e974f2d8004491e16032d4166ca8a515db0106f21a5c711b1ca1cec";
const testAccount2 = 'blacknet1pns3tp3wja8jmqqyfy0pvqedg9nv4zj3tkcpqmep5hr3rvw2rnkqhafpf9';
const bln = new blacknetjs();
const largeMessage = "Blacknet is extensible proof-of-stake network.  It's in development.  No premine. No ICO. No foundation.  Implementened features Accounts-based, Blacknet proof of stake version 4 Pooled cold staking Planned features Encrypted transfer message Multi-signature contract Atomic Swaps Permissionless extensions"

test('Withdraw From Lease', async () => {
    
    const res = await bln.jsonrpc.withdrawFromLease(testMnemonic2, {
        fee: 0.001*1e8,
        withdraw: 1*1e8,
        amount: 1*1e8,
        from: testAccount2,
        to: testAccount2,
        height: 10000
    });
    expect(res.body).toEqual('Transaction rejected: Can not withdraw more than -99900000000')
})

test('Cancel Lease', async () => {
    
    const res = await bln.jsonrpc.cancelLease(testMnemonic2, {
        fee: 0.001*1e8,
        amount: 1*1e8,
        from: testAccount2,
        to: testAccount2,
        height: 10000
    });
    expect(res.body).toEqual('Transaction rejected: Lease not found')
})

test('Lease', async () => {
    
    const res = await bln.jsonrpc.lease(testMnemonic2, {
        fee: 0.001*1e8,
        amount: 1*1e8,
        from: testAccount2,
        to: testAccount2
    });
    expect(res.body).toEqual('Transaction rejected: 100000000 less than minimal 100000000000')
})

test('Transfer', async () => {
    
    const res = await bln.jsonrpc.transfer(testMnemonic2, {
        amount: 0.01*1e8,
        message: 'blacknet',
        from: testAccount2,
        to: testAccount2,
        encrypted: 1
    });
    expect(res.code).toEqual(200)
})

test('Transfer largeMessage', async () => {
    
    const res = await bln.jsonrpc.transfer(testMnemonic2, {
        amount: 0.01*1e8,
        message: largeMessage,
        from: testAccount2,
        to: testAccount2,
        encrypted: 1
    });
    expect(res.code).toEqual(200)
})



test('Serialize With Transfer', async () => {
    
    const res = await bln.serialize.transfer({
        fee: 0.01*1e8,
        amount: 10*1e8,
        message: 'blacknet',
        from: testAccount2,
        to: testAccount2,
        encrypted: 1
    });
    expect(res.code).toEqual(200)
})

test('Serialize With Lease', async () => {
    
    const res = await bln.serialize.lease({
        fee: 0.01*1e8,
        amount: 10*1e8,
        from: testAccount2,
        to: testAccount2
    });
    expect(res.code).toEqual(200)
})

test('Serialize With Cancel Lease', async () => {
    
    const res = await bln.serialize.cancelLease({
        fee: 0.001*1e8,
        amount: 1*1e8,
        from: testAccount2,
        to: testAccount2,
        height: 1111111
    });
    expect(res.code).toEqual(200)
})

test('Serialize With Withdraw From Lease', async () => {
    
    const res = await bln.serialize.withdrawFromLease({
        fee: 0.001*1e8,
        amount: 1*1e8,
        withdraw: 1*1e8,
        from: testAccount2,
        to: testAccount2,
        height: 1111111
    });
    expect(res.code).toEqual(200)
})

test('Decrypt', () => {
    expect(blacknetjs.Decrypt(testMnemonic2, testAccount2, testEncrypted)).toEqual(testStr)
})

test('Encrypt And Decrypt', () => {
    const encrypt = blacknetjs.Encrypt(testMnemonic2, testAccount, testStr);
    const decrypt = blacknetjs.Decrypt(testMnemonic, testAccount2, encrypt);
    expect(decrypt).toEqual(testStr)

    // var fromKey = "prepare long erode easy moment dinosaur soft sound exhibit wire mesh muffin";
    // var fromAccount = blacknetjs.Address(fromKey);;
    // var toKey = "grape coconut enhance session educate round hole velvet liar harbor obey already";
    // var toAccount = blacknetjs.Address(toKey);;
    // var secretMsg  = 'its a secret msg';

    // let encryptMsg = blacknetjs.Encrypt(fromKey, toAccount, secretMsg);

    // let decryptMsg = blacknetjs.Decrypt(toKey, fromAccount, encryptMsg);

    // expect(decryptMsg).toEqual(secretMsg);
})

test('Generate Mnemonic with bip39', () => {
    expect(blacknetjs.Mnemonic().split(' ').length).toEqual(12)
})

test('Keypair with mnemonic', () => {
    const kp = {'publicKey': Uint8Array.from([171, 180, 189, 170, 132, 240, 214, 170, 138, 94, 25, 210, 15, 14, 98, 109, 30, 5, 133, 27, 10, 92, 202, 60, 126, 4, 166, 11, 225, 223, 52, 4]), 'secretKey': Uint8Array.from([30, 102, 46, 143, 195, 223, 137, 140, 200, 103, 119, 175, 91, 55, 17, 197, 17, 94, 212, 156, 48, 45, 71, 252, 177, 100, 206, 165, 241, 142, 162, 199, 171, 180, 189, 170, 132, 240, 214, 170, 138, 94, 25, 210, 15, 14, 98, 109, 30, 5, 133, 27, 10, 92, 202, 60, 126, 4, 166, 11, 225, 223, 52, 4])}
    expect(blacknetjs.ToKeypair(testMnemonic)).toEqual(kp)
})

test('Address with mnemonic', () => {
    expect(blacknetjs.Address(testMnemonic)).toBe(testAccount)
})

test('PublicKeyToAddress with publickey', () => {
    expect(blacknetjs.PublicKeyToAddress(Uint8Array.from([171, 180, 189, 170, 132, 240, 214, 170, 138, 94, 25, 210, 15, 14, 98, 109, 30, 5, 133, 27, 10, 92, 202, 60, 126, 4, 166, 11, 225, 223, 52, 4]))).toEqual(testAccount)
})

test('Signature with serialized', () => {
    expect(blacknetjs.Signature(testMnemonic, testSerialized)).toBe(testSignature)
})

test('SignMessage with message', () => {
    expect(blacknetjs.SignMessage(testMnemonic, testMessage)).toBe(testSign)
})

test('VerifyMessage with signature and message', () => {
    expect(blacknetjs.VerifyMessage(testAccount, testSign, testMessage)).toBe(true)
})