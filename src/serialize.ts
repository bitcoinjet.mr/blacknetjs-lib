import JsonRPC from './jsonrpc';
import RpcResponse from './jsonrpc-res';

import {Message} from './blacknet/core';

import Transaction, {Transfer} from './blacknet/transaction';
import Hash from './blacknet/core/hash';
import {ToHex} from './blacknet/utils';
import Lease from './blacknet/transaction/lease';
import CancelLease from './blacknet/transaction/cancellease';
import WithdrawFromLease from './blacknet/transaction/withdrawfromlease';

export default class {
    public jsonRPC: JsonRPC;
    constructor(jsonRPC: JsonRPC) {
        this.jsonRPC = jsonRPC;
    }

    public async transfer(data: {
        fee?: number,
        amount: number,
        message?: string,
        to: string,
        from: string,
        encrypted?: number
    }) {
        const body = await this.getSeq(data.from);
        if (body.code !== 200) {
            return body;
        }
        const m = new Message(Message.PLAIN, data.message);
        const d = new Transfer(data.amount, data.to, m);
        const ts = new Transaction(data.from, parseInt(body.body, 10), Hash.Empty(), data.fee, 0, d.Serialize());
        return new RpcResponse(200, ToHex(ts.Serialize()));
    }

    public async lease(data: {
        fee: number,
        amount: number,
        to: string,
        from: string
    }) {
        const body = await this.getSeq(data.from);
        if (body.code !== 200) {
            return body;
        }
        const d = new Lease(data.amount, data.to);
        const ts = new Transaction(data.from, parseInt(body.body, 10), Hash.Empty(), data.fee, 2, d.Serialize());
        return new RpcResponse(200, ToHex(ts.Serialize()));
    }

    public async cancelLease(data: {
        fee: number,
        amount: number,
        to: string,
        height: number,
        from: string
    }) {
        const body = await this.getSeq(data.from);
        if (body.code !== 200) {
            return body;
        }
        const d = new CancelLease(data.amount, data.to, data.height);
        const ts = new Transaction(data.from, parseInt(body.body, 10), Hash.Empty(), data.fee, 3, d.Serialize());
        return new RpcResponse(200, ToHex(ts.Serialize()));
    }

    public async withdrawFromLease(data: {
        fee: number,
        withdraw: number,
        amount: number,
        to: string,
        height: number,
        from: string
    }) {
        const body = await this.getSeq(data.from);
        if (body.code !== 200) {
            return body;
        }
        const d = new WithdrawFromLease(data.withdraw, data.amount, data.to, data.height);
        const ts = new Transaction(data.from, parseInt(body.body, 10), Hash.Empty(), data.fee, 11, d.Serialize());
        return new RpcResponse(200, ToHex(ts.Serialize()));
    }

    public async getSeq(account: string) {
        return await this.jsonRPC.get('/api/v1/walletdb/getsequence/' + account);
    }
}
