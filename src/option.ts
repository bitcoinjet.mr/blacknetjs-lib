
export default class Option {
    public endpoint: string;
    constructor(endpoint?: string ) {
        this.endpoint = endpoint || 'https://blnmobiledaemon.blnscan.io';
    }
}
