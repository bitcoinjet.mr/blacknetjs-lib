export default class RpcError extends Error {
    public json: any;
    constructor(json: any) {
        //   if (json.code) {
        //       super(json.message);
        //   } else {
        //       super(json);
        //   }
        super(json);
        Object.setPrototypeOf(this, RpcError.prototype);
        this.json = json;
    }
}
