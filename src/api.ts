import {randomFillSync} from 'crypto';
const chacha20 = require('chacha20');
const nacl     = require('./lib/nacl.js');
const util     = require('./lib/util.js');
const addr     = require('./lib/addr.js');
const bip39    = require('./lib/bip39.js');
const sigma    = 'Blacknet Signed Message:\n';
const hrp      = 'blacknet';

// PublicKeyToAddress
export function PublicKeyToAddress(publicKey: string | Buffer | Uint8Array): string | null {
    return addr.encode(hrp, typeof publicKey === 'string' ?
    Buffer.from(publicKey, 'hex').subarray(0, nacl.box_secretKeyLength) :
    publicKey.subarray(0, nacl.box_secretKeyLength)
    );
}

// Address
export function Address(mnemonic: string | Buffer | Uint8Array): string | null {

    const pair = ToKeypair(mnemonic);
    if (pair == null) {
        return null;
    }
    return addr.encode(hrp, Buffer.from(pair.publicKey));
}

// KeyPair
export function Keypair(mnemonic: string, skipCheckVerion = false): {
    publicKey: Uint8Array;
    secretKey: Uint8Array;
} | null {
    const sk = nacl.hash(util.decodeUTF8(mnemonic), nacl.box_secretKeyLength);
    if ( !skipCheckVerion && !util.checkVersion(sk) ) {
        return null;
    }

    return nacl.sign_keyPair_fromSeed(sk);
}

// PrivateToKeypair
export function PrivateToKeypair(privateKey: string | Buffer | Uint8Array): {
    publicKey: Uint8Array;
    secretKey: Uint8Array;
} {
    return nacl.sign_keyPair_fromSeed(Uint8Array.from(typeof privateKey === 'string' ?
    Buffer.from(privateKey, 'hex') :
    privateKey
    ).subarray(0, nacl.box_secretKeyLength));
}

// ToKeypair
export function ToKeypair(mnemonic: string | Buffer | Uint8Array): {
    publicKey: Uint8Array;
    secretKey: Uint8Array;
} {
    // check hexstring
    if (typeof mnemonic === 'string') {
        if (mnemonic.split(' ').length > 1) {
            return Keypair(mnemonic);
        }
    }
    return PrivateToKeypair(mnemonic);
}

// Mnemonic
export function Mnemonic(): string {

    const seed = new Uint16Array(12);
    let mnemonic = '';
    while (true) {
        util.getRandomValues(seed);
        for (let i = 0; i < 12; i++) {
            mnemonic += bip39.english[seed[i] % 2048];
            if ( i < 11 ) {
                mnemonic += ' ';
            }
        }
        const sk = nacl.hash(util.decodeUTF8(mnemonic), nacl.box_secretKeyLength);
        if ( util.checkVersion(sk) ) {
            break;
        }
        mnemonic = '';
    }
    nacl.cleanup(seed);

    return mnemonic;
}

// signMessage
export function SignMessage(mnemonic: string | Buffer | Uint8Array, message: string): string {

    const msg = util.decodeUTF8(sigma + message);
    const hashMessage = nacl.hash(msg, nacl.box_secretKeyLength);
    // const hash = nacl.hash(util.decodeUTF8(mnemonic), nacl.box_secretKeyLength);
    // const pair = nacl.sign_keyPair_fromSeed(hash);
    const pair = ToKeypair(mnemonic);

    return Buffer.from(
            nacl.sign(hashMessage, pair.secretKey).subarray(0, 64)
        )
        .toString('hex')
        .toUpperCase();
}

// verifyMessage
export function VerifyMessage(account: string, signature: string, message: string): boolean {

    const msg = util.decodeUTF8(sigma + message);
    const hashMessage = nacl.hash(msg, nacl.box_secretKeyLength);
    const publicKey = addr.decode(hrp, account);

    return nacl.sign_detached_verify(
        hashMessage,
        Uint8Array.from(Buffer.from(signature.toLowerCase(), 'hex')),
        Uint8Array.from(publicKey)
    );
}

// signature
export function Signature(mnemonic: string | Buffer | Uint8Array, serialized: string): string {

    // const mnemonicHash = nacl.hash(util.decodeUTF8(mnemonic), nacl.box_secretKeyLength);
    // const pair = nacl.sign_keyPair_fromSeed(mnemonicHash);
    const pair = ToKeypair(mnemonic);
    const signedMessage = Uint8Array.from(
        Buffer.from(serialized.toLowerCase(), 'hex')
    );
    const signedHash = nacl.hash(signedMessage.subarray(64), nacl.box_secretKeyLength);
    const signature = nacl.sign(signedHash, pair.secretKey).subarray(0, 64);
    for (let i = 0; i < 64; i++) {
        signedMessage[i] = signature[i];
    }

    return Buffer.from(signedMessage).toString('hex');
}

// decrypt
export function Decrypt(mnemonic: string | Buffer | Uint8Array, account: string, encrypt: string): string {

    // const mnemonicHash = nacl.hash(util.decodeUTF8(mnemonic), nacl.box_secretKeyLength);
    // const pair = nacl.sign_keyPair_fromSeed(mnemonicHash);
    const pair = ToKeypair(mnemonic);
    const publicKey = nacl.convertPublicKey(
        Uint8Array.from(addr.decode(hrp, account))
    );
    const secretKey = nacl.convertSecretKey(
        pair.secretKey
    );
    const key = nacl.x25519(
        secretKey,
        publicKey
    );
    const encryptByte = Uint8Array.from(
        Buffer.from(encrypt.toLowerCase(), 'hex')
    );
    const IV = encryptByte.subarray(0, 12);
    return chacha20.decrypt(key, IV, encryptByte.subarray(12)).toString('utf-8');
}

// encrypt
export function Encrypt(mnemonic: string | Buffer | Uint8Array, account: string, encrypt: string): string {

    // const mnemonicHash = nacl.hash(util.decodeUTF8(mnemonic), nacl.box_secretKeyLength);
    // const pair = nacl.sign_keyPair_fromSeed(mnemonicHash);
    const pair = ToKeypair(mnemonic);
    const publicKey = nacl.convertPublicKey(
        Uint8Array.from(addr.decode(hrp, account))
    );
    const secretKey = nacl.convertSecretKey(
        pair.secretKey
    );
    const key = nacl.x25519(
        secretKey,
        publicKey
    );
    const IV = nacl.randomBytes(12);
    const encryptByte = Uint8Array.from(
        Buffer.from(encrypt, 'utf-8')
    );
    const encrypted = Uint8Array.from(chacha20.encrypt(key, IV, encryptByte));
    const out = new Uint8Array(12 + encrypted.length);
    out.set(IV);
    out.set(encrypted, IV.length);

    return Buffer.from(out).toString('hex').toLocaleUpperCase();
}
