import RpcResponse from './jsonrpc-res';
import serialize from './serialize';
import { Signature } from './api';
import Blacknetjs from '.';

const FormData = 'FormData' in global ? (global as any).FormData : require('form-data');

export default class {
    public endpoint: string;
    public serialize: serialize;
    public fetchBuiltin: (input?: Request | string, init?: RequestInit) => Promise<Response>;

    constructor(endpoint: string, args:
        { fetch?: (input?: string | Request, init?: RequestInit) => Promise<Response> } = {},
    ) {
        this.endpoint = endpoint;
        if (args.fetch) {
            this.fetchBuiltin = args.fetch;
        } else if ('fetch' in global) {
            this.fetchBuiltin = (global as any).fetch.bind(global);
        } else {
            this.fetchBuiltin = require('node-fetch');
        }
        this.serialize = new serialize(this);
    }

    public async fetch(path: string, config?: object) {
        return this.fetchBuiltin(this.endpoint + path, config);
    }

    public async post(path: string, body?: any, option?: object) {
        const formdata = new FormData();
        option = option || {
            mode: 'cors'
        };
        if (body) {
            Object.keys(body).forEach((key) => {
                formdata.append(key, body[key]);
            });
        }
        const response = await this.fetch(path, Object.assign(option, {
            body: formdata,
            method: 'POST'
        }));
        try {
            return new RpcResponse(response.status, await response.text());
        } catch (e) {
            return new RpcResponse(500, e.message);
        }
    }

    public async get(path: string, option?: object) {
        option = option || {
            mode: 'cors'
        };
        const response = await this.fetch(path, Object.assign(option, {
            method: 'GET'
        }));
        try {
            return new RpcResponse(response.status, await response.text());
        } catch (e) {
            return new RpcResponse(500, e.message);
        }
    }

    public async transfer(mnemonic: string | Buffer | Uint8Array, data: {
        fee?: number,
        amount: number,
        message?: string,
        to: string,
        from: string,
        encrypted?: number
    }) {

        if (data.encrypted === 1) {
            data.message = Blacknetjs.Encrypt(mnemonic, data.to, data.message);
        }
        data.fee = parseInt(this.getFees(data.message).toString(), 10);
        const serializedRes = await this.serialize.transfer(data);
        if (serializedRes.code !== 200) {
            return serializedRes;
        }
        const signature = Signature(mnemonic, serializedRes.body);

        return await this.get([
            '/api/v2/sendrawtransaction',
            signature
        ].join('/'));
    }

    public async lease(mnemonic: string | Buffer | Uint8Array, data: {
        fee: number,
        amount: number,
        to: string,
        from: string
    }) {
        const serializedRes = await this.serialize.lease(data);
        if (serializedRes.code !== 200) {
            return serializedRes;
        }
        const signature = Signature(mnemonic, serializedRes.body);
        return await this.get([
            '/api/v2/sendrawtransaction',
            signature
        ].join('/'));
    }

    public async getSeq(account: string) {

        return await this.get('/api/v1/walletdb/getsequence/' + account);
    }

    private getFees(message: string): number {

        const minTxFee = 100000;
        if (!message) {
            return minTxFee;
        }
        const normalSize = 184;
        const messageSize = Buffer.from(message, 'utf-8').length;
        const total = normalSize + messageSize;

        return minTxFee * (1 + total / 1000);
    }

    public async cancelLease(mnemonic: string | Buffer | Uint8Array, data: {
        fee: number,
        amount: number,
        to: string,
        height: number,
        from: string
    }) {

        const serializedRes = await this.serialize.cancelLease(data);
        if (serializedRes.code !== 200) {
            return serializedRes;
        }
        const signature = Signature(mnemonic, serializedRes.body);
        return await this.get([
            '/api/v2/sendrawtransaction',
            signature
        ].join('/'));
    }

    public async withdrawFromLease(mnemonic: string | Buffer | Uint8Array, data: {
        fee: number,
        withdraw: number,
        amount: number,
        to: string,
        height: number,
        from: string
    }) {

        const serializedRes = await this.serialize.withdrawFromLease(data);
        if (serializedRes.code !== 200) {
            return serializedRes;
        }
        const signature = Signature(mnemonic, serializedRes.body);
        return await this.get([
            '/api/v2/sendrawtransaction',
            signature
        ].join('/'));
    }
}
