import {EncodeVarInt, DecodeVarInt} from '../utils';

class Message {
    public Type: number;
    public Message: string;
    constructor(type: number , message?: string) {
        this.Type = type;
        this.Message = message;
    }
    // Serialize
    public Serialize(): Uint8Array {
        const type = Uint8Array.from([this.Type]);
        const msg = this.Message ? Uint8Array.from(
            Buffer.from(this.Message, 'utf-8')
        ) : Uint8Array.from([]);
        const len = EncodeVarInt(msg.length);
        return Uint8Array.from([...type, ...len, ...msg]);
    }
    // Derialize
    public static Derialize(arr: Uint8Array): Message {
        const type = Buffer.from(arr.subarray(0, 1)).readUInt8(0);
        return new Message(
            type,
            Buffer.from(
                arr.subarray(arr.length - DecodeVarInt(arr.subarray(1)))
            ).toString('utf-8')
        );
    }
    public static Empty(): Message {
        return new Message(Message.PLAIN);
    }
    public static PLAIN = 0;
    public static ENCRYPTED = 1;
}

export default Message;
