import cancellease from './cancellease';
import lease from './lease';
import transfer from './transfer';
import Signature from '../core/signature';
import Hash from '../core/hash';
import {HexToPublicKey, PublicKey, PublicKeyToHex, PublicKeyToAccount} from '../utils';
import {EncodeVarInt, DecodeVarInt} from '../utils';

export const CancelLease = cancellease;
export const Lease = lease;
export const Transfer = transfer;

const Uint64BE = require('int64-buffer').Uint64BE;

class Transaction {
    public Signature: Signature;
    public From: string;
    public Seq: number; // uint32
    public Hash: Hash;
    public Fee: number; // uint64
    public Type: number; // uint8
    public Data: Uint8Array;
    constructor(from: string, seq: number, hash: Hash, fee: number, type: number, data: Uint8Array) {
        this.Signature = Signature.Empty();
        this.From = PublicKeyToHex(PublicKey(from));
        this.Seq = seq;
        this.Hash = hash;
        this.Fee = fee;
        this.Type = type;
        this.Data = data;
    }
    // Serialize
    public Serialize(): Uint8Array {
        const signature = this.Signature.Bytes();
        const from = HexToPublicKey(this.From);
        const seq = new Uint8Array(new Uint64BE(this.Seq).buffer).subarray(4, 8);
        const hash = this.Hash.Bytes();
        const fee = new Uint8Array(new Uint64BE(this.Fee).buffer);
        const type = Uint8Array.from([this.Type]);
        const l = EncodeVarInt(this.Data.length);
        const data = this.Data;
        return Uint8Array.from([
            ...signature,
            ...from,
            ...seq,
            ...hash,
            ...fee,
            ...type,
            ...l,
            ...data
        ]);
    }
    // Derialize
    public static Derialize(arr: Uint8Array): Transaction {
        const from = PublicKeyToAccount(arr.subarray(64, 96));
        const seq  = new Uint64BE(arr.subarray(96, 100)).toNumber();
        const fee  = new Uint64BE(arr.subarray(132, 140)).toNumber();
        const type = new Uint64BE(arr.subarray(140, 141)).toNumber();
        const len  = DecodeVarInt(arr.subarray(141));
        const data = arr.subarray(arr.length - len, arr.length);
        return new Transaction(from, seq, new Hash(arr.subarray(100, 132)), fee, type, data);
    }
}

export default Transaction;
