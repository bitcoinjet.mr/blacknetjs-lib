import Message from '../core/message';
import {HexToPublicKey, PublicKey, PublicKeyToHex, PublicKeyToAccount} from '../utils';

const Uint64BE = require('int64-buffer').Uint64BE;

class Transfer {
    public Amount: number;
    public To: string;
    public Message: Message;
    constructor(amount: number, to: string, message: Message) {
        this.Amount = amount;
        this.To = PublicKeyToHex(PublicKey(to));
        this.Message = message;
    }
    // Serialize
    public Serialize(): Uint8Array {
        const amount = new Uint64BE(this.Amount).buffer;
        const to = HexToPublicKey(this.To);
        const message = this.Message.Serialize();
        return Uint8Array.from([
            ...amount,
            ...to,
            ...message
        ]);
    }
    // Derialize
    public static Derialize(arr: Uint8Array): Transfer {
        const amount = new Uint64BE(arr.subarray(0, 8)).toNumber();
        const to = PublicKeyToAccount(arr.subarray(8, 40));
        return new Transfer(amount, to, Message.Derialize(arr.subarray(40)));
    }
}

export default Transfer;
