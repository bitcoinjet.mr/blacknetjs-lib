import {HexToPublicKey, PublicKey, PublicKeyToHex, PublicKeyToAccount} from '../utils';

const Uint64BE = require('int64-buffer').Uint64BE;

class CancelLease {
    public Amount: number;
    public To: string;
    public Height: number;
    constructor(amount: number, to: string, height: number) {
        this.Amount = amount;
        this.To = PublicKeyToHex(PublicKey(to));
        this.Height = height;
    }
    // Serialize
    public Serialize(): Uint8Array {
        const amount = new Uint64BE(this.Amount).buffer;
        const to = HexToPublicKey(this.To);
        const height = new Uint8Array(new Uint64BE(this.Height).buffer).subarray(4, 8);
        return Uint8Array.from([
            ...amount,
            ...to,
            ...height
        ]);
    }
    // Derialize
    public static Derialize(arr: Uint8Array): CancelLease {
        const amount = new Uint64BE(arr.subarray(0, 8)).toNumber();
        const to = PublicKeyToAccount(arr.subarray(8, 40));
        const height = new Uint64BE(arr.subarray(40)).toNumber();
        return new CancelLease(amount, to, height);
    }
}

export default CancelLease;
